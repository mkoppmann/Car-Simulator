#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Component which represents a tank. Inherits from component class.
"""

import random

from component import Component
from pid import Pid


class Tank(Component):
    def __init__(self):
        """
        Mode: 0x01 - current data
        Pid: 0x2F - tank
        Value: 100 - full tank (in percent)
        """
        self.pid = Pid(0x01, 0x2F, 100)
        random.seed()

    def get_value(self):
        return self.pid.value

    def set_value(self, value):
        if 0 <= value <= 100:
            self.pid.value = value

    def randomize(self):
        self.set_value(random.randint(0, 100))
