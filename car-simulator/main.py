#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Entry point for the application. Starts all components and the canserver by default. Another interfaces can be used to start the program but currently there is no logic behind that (means, if something else than vcan is used, the application will crash). It gets messages from the canserver, checks which component is responsible for that, gets the corresponding current value and sends it back via the canserver.
"""

import sys

from battery import Battery
from canserver import CanServer
from car import Car
from tank import Tank

if __name__ == '__main__':

    # Get interface
    if len(sys.argv) == 1:
        interface = 'vcan0'
    else:
        interface = sys.argv[1]

    battery = Battery()
    car = Car()
    tank = Tank()
    server = CanServer(interface)

    print('Simulator ready')

    # Endless loop, main application logic
    while True:
        # Reset the values which each new request
        car.randomize()
        battery.randomize()
        tank.randomize()

        # Get new can message (blocking method)
        message = server.listen()

        # If the message was not valid, restart the loop
        if message is None:
            pass

        # Get the OBD2 mode and pid from the message
        req_mode = message.data[1]
        req_pid = message.data[2]

        # Currently only mode 1 (current data) is supported
        if req_mode != 1:
            pass

        # Define which component corresponds to which pid
        components = {
            0x0d: car,
            0x5b: battery,
            0x2f: tank,
        }

        # Request current value from the requested component and send it back
        value = components[req_pid].get_value()
        server.send_message(req_pid, value)
