#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Component which represents a car and contains the value of the current speed. Inherits from component class.
"""

import random

from component import Component
from pid import Pid


class Car(Component):
    def __init__(self):
        """
        Mode: 0x01 - current data
        Pid: 0x0D - speed
        Value: 130 - km/h (min 0 to max 255)
        """
        self.pid = Pid(0x01, 0x0D, 130)
        random.seed()

    def get_value(self):
        return self.pid.value

    def set_value(self, value):
        if 0 <= value <= 130:
            self.pid.value = value

    def randomize(self):
        self.set_value(random.randint(0, 130))
