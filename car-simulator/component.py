#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Abstract class which represents a car component with a value, which can be requested by OBD2
"""

from abc import ABC, abstractmethod

from pid import Pid


class Component(ABC):
    """
    Constructor, values are different for each component.
    Get values from here: https://en.wikipedia.org/wiki/OBD-II_PIDs#Mode_01
    """
    @abstractmethod
    def __init__(self):
        """
        Mode: 0x01 - current data
        Pid: 0x00 - default
        Value: 0x00 - default
        """
        self.pid = Pid(0x01, 0x00, 0x00)

    # Simply returns the current value
    def get_value(self):
        return self.pid.value

    # Sets a new value. Optionally check if the value is in the right range
    def set_value(self, value):
        self.pid.value = value

    # Method which randomizes the current value. Use a valid range for the OBD2 pid.
    @abstractmethod
    def randomize(self):
        pass
