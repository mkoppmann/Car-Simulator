#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Represents a OBD2 Pid object.
"""

# Mode 01 shows current data
STANDARD_MODE = 0x01


class Pid:
    def __init__(self, mode=STANDARD_MODE, pid=0x00, value=0):
        self.mode  = mode
        self.pid   = pid
        self.value = value
