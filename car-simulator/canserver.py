#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Communication server for the can interface. Uses the native socketcan interface available in Python 3.4 and higher. Listens to new messages and sends responds back.
"""

import can


class CanServer:
    # Constructor, initializes can bus
    def __init__(self, interface):
        self.bus1 = can.interface.Bus(interface, bustype="socketcan_native")
        print('Listening on ' + str(interface) + ' interface')

    # Sends a OBD2 respond back. Format is hardcoded for mode 1 (current data)
    def send_message(self, pid, value):
        """
        See https://en.wikipedia.org/wiki/OBD-II_PIDs#Response
        """
        msg = can.Message(arbitration_id=0x07E8, data=[3, 0x41, pid, value, 0x00, 0x00, 0x00, 0x00])
        self.bus1.send(msg)
        print('Sent: ' + str(msg))

    # Listens for new can messages and checks if they have the right format
    def listen(self):
        recv_message = self.bus1.recv()
        print('Received: ' + str(recv_message))

        # 0x07DF is a request message in CAN
        if recv_message.arbitration_id == 0x07DF:
            return recv_message
        else:
            print('Invalid request received.')
            return None
