#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Component which represents a hybrid car battery. Inherits from component class.
"""

import random

from component import Component
from pid import Pid


class Battery(Component):
    def __init__(self):
        """
        Mode: 0x01 - current data
        Pid: 0x5B - battery
        Value: 100 - full tank (in percent)
        """
        self.pid = Pid(0x01, 0x5B, 100)
        random.seed()

    def get_value(self):
        return self.pid.value

    def set_value(self, value):
        if 0 <= value <= 100:
            self.pid.value = value

    def randomize(self):
        self.set_value(random.randint(0, 100))
