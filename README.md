# car-simulator

This is a basic car-simulator which understands some OBD2 requests.

## Setup

This simulator requires Python 3 (version 3.4 and higher for the native can interface). Install the requirements with `pip install -r requirements.txt`.

## How to run

Start the simulator by running `python3 main.py`. It listens per default on the `vcan0` interface.

The simulator supports the following three commands:

* Get current tank status    (between 0 and 100%)
* Get current battery status (between 0 and 100%)
* Get current speed          (between 0 and 130 km/h)

To request this information, you must send CAN messages. E.g. to get the current speed, you have to send: `cansend vcan0 "7DF#02010D5555555555"`. This follows the OBD2 specification. `0D` in this case represent the pid for the current speed. To get the current tank or battery status, use `2F` and `5B` respectively.

The simulator will respond with a message like this: `vcan0  000007E8   [8]  03 41 0D 1A 00 00 00 00`. The `0D` confirms the requested pid and `1A` represents the value in hex (26 km/h in this case). The values of the components are randomized with each request.

## File structure

* main.py      - Entry point for the application.
* pid.py       - Represents a OBD2 pid with mode, pid and value.
* canserver.py - Manages can messages. Listens to new messages and sends responses.
* component.py - Abstract class which represents a component which can react to OBD2 requests.
  * car.py     - Component which contains the current speed.
  * tank.py    - Component which contains the current fuel status.
  * battery.py - Component which contains the current hybrid battery status.

## Todo

Connect to an OBD2 Bluetooth adapter instead of using vcan. Connecting a Raspberry Pi to one of these adapters via Bluetooth worked but communication with the interface was error prone and did not deliver meaningful results.

## Useful documentation

To get all official pids with the right specification, see <https://en.wikipedia.org/wiki/OBD-II_PIDs#Mode_01>
